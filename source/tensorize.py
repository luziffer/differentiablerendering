# tensorize.py
# 
# Luckily numpy and pytorch are both similar but sadly not identical.
# This module provides a numpy interface for pytorch.
# Here is a list of useful modifications their reasons: 
#
# - floating point types
# - automatic CPU/GPU conversions:
#       initializations have .to(device) 
#       (although it could be brute-forced in every wrapper)
# - easy debugging! in every operation gradients, dtypes, shape etc. can be checked.
# 

import torch
import numpy as np


#default floating-point,binary and integer types
from torch import float32 as float
from torch import uint8 as bool
from torch import int32 as int

#default device
device = 'cuda:0' if torch.cuda.is_available() else 'cpu'
device = torch.device(device)
if __name__ == '__main__':
    print('Running on device "%s"' % device)


def zeros(shape, dtype=float, requires_grad=False):    
    return torch.zeros(shape, dtype=dtype,requires_grad=requires_grad).to(device)

def ones(shape, dtype=float, requires_grad=False):
    return torch.ones(shape, dtype=dtype, requires_grad=requires_grad).to(device)

def astensor(values, dtype=None):
    if isinstance(values, torch.Tensor):
        assert False, 'should not be used for tensors'
    if dtype is None:
        auto = torch.as_tensor(values)
        if auto.is_floating_point():
            dtype = float
        else: dtype = int
    return torch.as_tensor(values, dtype=dtype).to(device) #explicitally init in device!

from torch import tensor

def astype(values, dtype): #does this work??
    return tensor(values, dtype=dtype) #.to(device)

from torch import is_floating_point

def repeat(values, n, axis=None):
    return torch.repeat_interleave(values, n, dim=axis)#.to(device)

from torch import stack

def random_normal(size, mean=astensor(0.), variance=astensor(1.)):
    epsilon = astype( torch.randn(size), float ).to(device) 
    return epsilon * variance + mean

from torch import where, nonzero

def sum(values, axis=None):
    if axis is None:
        return torch.sum(values)#.to(device)    
    return torch.sum(values, dim=axis)#.to(device)

def all(values, axis=None):
    if axis is None:
         return values.all()#.to(device)
    return values.all(dim=axis)#.to(device)

def any(values, axis=None):
    return values.any(dim=axis)#.to(device)

from torch import exp,log,sin,cos,tan,sqrt,linspace,abs

def cross(vec1, vec2, axis=None):
    if axis is None:
        return vec1.cross(vec2) #.to(device)
    return vec1.cross(vec2, dim=axis) #.to(device)

def max( values, axis=None ):
    if axis is None:
        return torch.max(values)
    return torch.max(values, axis=axis)[0]

def minimum(values1, values2):
    return torch.where( values1 <= values2, values1, values2).to(device)

def maximum(values1, values2):
    return torch.where( values1 >= values2, values1, values2).to(device)

def detach_numpy(tensor):
    return tensor.cpu().detach().numpy()
 
from torch.nn.functional import conv2d