import source.tensorize as t
import matplotlib.pyplot as plt


class Image:

    def __init__(self, depth_buffer, color_buffer, camera_name, width, height):
        self.depth_buffer = depth_buffer.reshape(width, height)

        self.color_buffer = color_buffer
        b_color = self.color_buffer
        t_0, t_1 = t.zeros(b_color.shape), t.ones(b_color.shape)
        b_color = t.maximum(t_0, t.minimum(t_1, b_color))
        self.color_buffer = b_color.reshape((width, height, 3))

        self.width = width
        self.height = height
        self.camera_name = camera_name

    def get_image(self, image_type='depth'):
        if image_type == 'depth':
            return self.depth_buffer
        elif image_type == 'binary':
            return self.depth_buffer > 1e-6
        elif image_type == 'albedo':
            return self.color_buffer
        raise AttributeError("The image type '{0}' is not defined.".format(image_type))

    def show_image(self, image_type='depth', title="", subplot=None):
        image = t.detach_numpy(self.get_image(image_type))
        
        if image_type == 'albedo':
            cmap = None
            image = image.transpose((1,0,2))    # we store pictures in width x height index order
            image = image[::-1,:,:] #we treat "up" direction as positive. whereas 
                                    #imshow() treats "up" direction as as negative 
        elif image_type == 'depth':
            cmap = 'gray'
            image = image.transpose()
        else:
            raise NotImplementedError('other buffer image types than "depth", "albedo" not available')
        
        if subplot is not None:
            plt.subplot(subplot)
        plt.imshow(image, cmap=cmap)
        plt.title(title)
