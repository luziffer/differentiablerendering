#!/usr/bin/env python3
# -*- coding: utf-8 -*-

if __name__ == '__main__':
    import sys
    sys.path.append('../')
    
import numpy as np
from source.scene import * 
import source.tensorize as t 

__all__ = ['intersect_rays_with_sphere',
           'intersect_rays_with_plane',
           'intersect_rays_with_triangle',
           'construct_initial_rays',
           'tensorize_scene_object']


def tensorize_scene_object(obj, requires_grad=True):
    tensorized = dict()
    for field in obj._fields:
        value = eval('obj.'+field)
        value = t.astensor(value)
        if t.is_floating_point(value):
            value.requires_grad_(True)
        tensorized[field] =value
        #tensorized[field] = t.tensor(tensorized[field],requires_grad=requires_grad)
        
    return tensorized

if __name__ == '__main__':
    ball = Plane()
    print('Unit Test: Tensorization')
    print('========================')
    print('- Untensorized: \n', ball, '\n')
    ball = tensorize_scene_object(ball)
    print('- Tensorized: \n', ball, '\n')
    

### Ray Intersections
#
# Calculate the intersections of rays with scene objects.
# For these we get a distance from the ray support points to the scene object.
# If the returned distance is non-positive it means that there was no intersection.


def intersect_rays_with_sphere(rays, sphere, return_normal=False):
    s,d = rays
    
    center = sphere['center'][None,:]
    radius = sphere['radius']
    
    #quadratic coefficients
    a = t.sum( d**2, axis=-1 )
    b = t.sum( d * (s - center), axis=-1)
    c = t.sum( (s - center)**2, axis=-1 ) - radius**2
    
    
    #calculate intersection distance
    intersection = b**2 > a*c + 1e-6
    residual = t.sqrt( t.maximum( t.zeros(b.shape), b**2-a*c) + 1e-6 )
    dist = - t.ones(intersection.shape)
    dist_back  = t.where(intersection, (-b+residual)/a, - t.astensor(1.))
    dist_front = t.where(intersection, (-b-residual)/a, - t.astensor(1.))
    dist = t.where(intersection & (dist_back > 0), dist_back, dist)
    dist = t.where(intersection & (dist_front > 0), dist_front, dist)
    if not return_normal: return dist
    
    #calculate normal
    intersection = dist > 0
    intersection = intersection[:,None]
    
    s_next = s + dist[:,None] * d
    normal = t.where(intersection, s_next - center, t.astensor(0.))
    normal = t.where(intersection, normal / t.sqrt(t.sum(normal**2, axis=-1)+ 1e-9)[:,None],-t.astensor(1.))
    
    return dist, normal

#unit tests
if __name__=='__main__':
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    #Test: Sphere
    print('Unit Test: Sphere')
    fig = plt.figure(figsize=(8,6))
    ax = plt.axes(projection='3d')
    ax.set_aspect('auto')
    plt.title('Ray hits Sphere')

    #depict default sphere
    sphere = Sphere()
    u = np.linspace(0, 2 * np.pi, 100)
    v = np.linspace(0, np.pi, 100)
    r = sphere.radius
    center = sphere.center
    x = r * np.outer(np.cos(u), np.sin(v)) + center[0]
    y = r * np.outer(np.sin(u), np.sin(v)) + center[1]
    z = r * np.outer(np.ones(np.size(u)), np.cos(v)) + center[2]
    ax.plot_surface(x, y, z,  rstride=4, cstride=4, color='k', linewidth=0, alpha=0.1)

    #tensorize inputs
    s = t.astensor([[3.,0,0]])
    d = t.astensor([[-.984,.315,0]])
    sphere = tensorize_scene_object(sphere)
    
    #autograd unit test
    print('ray support: ', s[0], s.dtype, s.requires_grad)
    print('ray direction: d=', d[0], s.dtype, s.requires_grad)
    for field in sphere:
        val = sphere[field]
        print(field,':', val.dtype, val.requires_grad)
    
    #calculate ray intersections
    p,n = intersect_rays_with_sphere((s,d), sphere, return_normal=True)
    s,d,p,n = s[0], d[0], p[0], n[0]
    x = s + p * d
    
    s,d,p,n,x = tuple(t.detach_numpy(i) for i in [s, d, p, n, x])
    
    #create output
    plt.plot([s[0], s[0]+d[0]], [s[1], s[1]+d[1]], [s[2], s[2]+d[2]], color='blue', label='ray direction')
    plt.plot([s[0], x[0]], [s[1], x[1]], [s[2], x[2]], color='blue', alpha=.3, label='intersection path')   
    plt.plot([x[0], x[0]+n[0]], [x[1],x[1]+n[1]], [x[2], x[2]+n[2]], color='orange', label='normal at intersection')
    plt.legend()
    plt.savefig('../images/intersection_test_sphere.svg', transparent=True)
    plt.show()
    


def intersect_rays_with_plane(rays, plane, return_normal=False):
    s,d = rays
    normal = plane['normal'][None, :]
    offset = plane['offset']
    
     #linear coefficients
    a = t.sum( normal * d, axis=-1 ) 
    b = t.sum( normal * s, axis=-1 ) - offset
    
    #calculate distance
    intersection = t.abs(a) > 1e-3
    dist = t.where(intersection, a, t.astensor(1e-3))
    dist = -b/dist
    dist = t.where(dist>0, dist, t.astensor(-1.))
    if not return_normal: return dist
    
    normal = t.repeat(normal,  len(dist), axis=0)
    return dist, normal
    

#unit tests
if __name__=='__main__':
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    
    #Test: Plane
    print('Unit Test: Plane')
    fig = plt.figure(figsize=(8,6))
    ax = plt.axes(projection='3d')
    ax.set_aspect('auto')
    plt.title('Ray hits Plane')

    #depict default plane
    plane = Plane()
    n = plane.normal
    offset = plane.offset
    x,y = 2*np.mgrid[:2,:2]-1
    z = offset[None] +  n[0]*x + n[1]*y
    z = - z / (n[2] + 1e-9)
    ax.plot_surface(x, y, z,  rstride=4, cstride=4, color='k', linewidth=0, alpha=0.1)

    #calculate ray intersections
    s = t.astensor([[0,0,2.]])
    d = t.astensor([[.2,0,-1]])
    plane = tensorize_scene_object(plane)
    p,n = intersect_rays_with_plane((s,d), plane, return_normal=True)
    s,d,p,n = s[0], d[0], p[0], n[0]
    x = s + p * d
    
    s,d,p,n,x = tuple(t.detach_numpy(i) for i in [s, d, p, n, x])
    
    #create output
    plt.plot([s[0], s[0]+d[0]], [s[1], s[1]+d[1]], [s[2], s[2]+d[2]], color='blue', label='ray direction')
    plt.plot([s[0], x[0]], [s[1], x[1]], [s[2], x[2]], color='blue', alpha=.3, label='intersection path')   
    plt.plot([x[0], x[0]+n[0]], [x[1],x[1]+n[1]], [x[2], x[2]+n[2]], color='orange', label='normal at intersection')
    plt.legend()
    
    plt.xticks([-.8,-.4,.0,.4,.8,1.2])
    plt.yticks([-.8,-.4,.0,.4,.8,1.2])
    plt.gca().set_zticks([0,.5,1,1.5,2.])

    plt.savefig('../images/intersection_test_plane.svg', transparent=True)
    plt.show()



def intersect_rays_with_triangle(rays, triangle, return_normal=False):
    s,d = rays
    a = triangle['a']
    b = triangle['b']
    c = triangle['c']

    #supporting center point and normal vector
    m = (a+b+c)/3.
    t1 = a-b
    t2 = c-b
    n = -t.cross(t1, t2)
    n = n / t.sqrt( t.sum(n**2) + 1e-6)
    
    #point normal towards ray
    flip_normal = d * n[None,:]
    flip_normal = t.sum( flip_normal, axis=-1)
    flip_normal = flip_normal > 0
    flip_normal = t.where(flip_normal, t.astensor(-1.), t.astensor(1.))
    n = flip_normal[:,None] * n
    
    #linear coefficients
    f = t.sum( n * d, axis=-1 ) 
    g = t.sum( n * (s-m[None,:]), axis=-1 )
    
    #calculate distance as plane
    intersection = t.abs(f) > 1e-3
    dist = t.where(intersection, f, t.astensor(1e-3))
    dist = - g/dist
    intersection = intersection & (dist>1e-3)
    
    x = s + dist[:,None]*d   
    y = x - b[None,:]
     
    h = t.sum(t1**2)*t.sum(t2**2) - t.sum(t1*t2)**2
    h1 = t.sum(t2*t2) * t.sum(y * t1[None,:], axis=-1) - t.sum(t2*t1) * t.sum(y * t2[None,:], axis=-1)
    h2 = t.sum(t1*t1) * t.sum(y * t2[None,:], axis=-1) - t.sum(t1*t2) * t.sum(y * t1[None,:], axis=-1)
    h1 = h1 / h
    h2 = h2 / h
    
    intersection = intersection & (h1>0) & (h2>0) & (h1+h2 < 1.)
   
    
#    #test against three half planes
#    gamma,beta,alpha = (a+b)/2., (a+c)/2.-m, (b+c)/2.-m
#    cond_alpha = t.sum( (x - m[None,:]) * alpha[None,:], axis=-1) <= t.sum(alpha**2)
#    cond_beta  = t.sum( (x - m[None,:]) *  beta[None,:], axis=-1) <= t.sum(beta**2)
#    #cond_gamma = t.sum( (x - gamma[None,:]) * (gamma-m)[None,:], axis=-1) <  0.
#    intersection = intersection & cond_alpha
#    intersection = intersection & cond_beta
#    #intersection = intersection & cond_gamma
    
#    cond_a = t.sum( (x-m[None,:])*((a-m)[None,:]), axis=-1) > -1.
#    cond_b = t.sum( (x-m[None,:])*((b-m)[None,:]), axis=-1) > -1.
#    cond_c = t.sum( (x-m[None,:])*((c-m)[None,:]), axis=-1) > -1.
#    intersection = intersection & cond_a
#    intersection = intersection & cond_b
#    intersection = intersection & cond_c
    
    
    dist = t.where(intersection, dist, t.astensor(-1.))
    if not return_normal: return dist
    
    return dist, n
    

#unit tests
if __name__=='__main__':
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    #Test: Triangle
    print('Unit Test: Triangle')
    fig = plt.figure(figsize=(8,6))
    ax = plt.axes(projection='3d')
    ax.set_aspect('auto')
    plt.title('Ray hits Triangle')

    #depict default plane
    tri = Triangle()
    a,b,c = tri.a, tri.b, tri.c
    x,y,z = [a[0],b[0],c[0]], [a[1],b[1],c[1]], [a[2],b[2],c[2]]
    x,y,z = np.asarray(x), np.asarray(y), np.asarray(z)
    ax.plot_trisurf(x, y, z, color='k', alpha=0.1)
    

    #calculate ray intersections
    s = t.astensor([[0.4,1.6,5.5]])
    d = t.astensor([[0.0,-0.2,-1]])
    d = d / t.sum( d**2 )
    tri = tensorize_scene_object(tri)
    p,n = intersect_rays_with_triangle((s,d), tri, return_normal=True)
    s,d,p,n = s[0], d[0], p[0], n[0]
    x = s + p * d
    
    s,d,p,n,x = tuple(t.detach_numpy(i) for i in [s, d, p, n, x])

    #create output
    plt.plot([s[0], s[0]+d[0]], [s[1], s[1]+d[1]], [s[2], s[2]+d[2]], color='blue', label='ray direction')
    plt.plot([s[0], x[0]], [s[1], x[1]], [s[2], x[2]], color='blue', alpha=.3, label='intersection path')   
    plt.plot([x[0], x[0]+n[0]], [x[1],x[1]+n[1]], [x[2], x[2]+n[2]], color='orange', label='normal at intersection')
    plt.legend()
    #plt.xticks([0,.5,1])
    plt.yticks([0,.5,1,1.5])
    #plt.gca().set_zticks([0,.5,1])
    
    plt.savefig('../images/intersection_test_triangle.svg', transparent=True)
    plt.show()



### Camera Initialization
#
# We can construct the rays depending on what kind of camera and lens we want.
# For now just a normal perspective camera.

def construct_initial_rays(camera):
    
    #read camera parameters
    W = camera['width'].cpu().numpy()
    H = camera['height'].cpu().numpy()
    fov = 3.1415 / 180.0 * camera['field_of_view']
    f = camera['focal_length']
    yaw = camera['yaw']
    pitch = camera['pitch']
    
    #create indices    
    ii,jj = np.mgrid[:W,:H]
    ii = t.astensor(ii, t.float)
    jj = t.astensor(jj, t.float)
    w = t.astype(W, t.float)
    h = t.astype(H, t.float)
    
    #screen coordinates
    s = t.stack([
        t.sin(fov)     * (2.*ii/(w-1.) -1. ),
        t.sin(fov)*h/w * (2.*jj/(h-1.) -1. ),
        - f * t.ones((W,H)),
    ], dim=-1)
    s = s.reshape((-1,3))
    
    yc, ys, pc, ps = t.cos(yaw), t.sin(yaw), t.cos(pitch), t.sin(pitch)
    s = t.astensor([
        [ yc*pc, ps, ys*pc], 
        [    0., pc,   0.],
        [-ys*pc, ps, yc*pc],
    ])[None,:,:] * s[:, None,:]
    s = t.sum(s, axis=-1)
    
    #normalize to get directional 
    d = s / t.sqrt( t.sum( s**2, axis=-1) + 1e-9) [:,None]
    
    #translate camera
    s = s - camera['position'][None,:]
    
    
    return s,d

#unit tests
if __name__=='__main__':
    import matplotlib.pyplot as plt

    #Test: Camera
    print('Unit Test: Camera')
    fig = plt.figure(figsize=(8,6))
    plt.title("camera's supporting vector's x component")
    
    #initialize rays
    camera = Camera()
    camera = tensorize_scene_object(camera)
    w,h = camera['width'], camera['height']
    s,d = construct_initial_rays(camera)
    s = s.reshape((w,h,3))
    d = s.reshape((w,h,3))
    plt.imshow(t.detach_numpy(s)[:,:,1], cmap='gray')
    
  
    plt.show()

