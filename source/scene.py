#!/usr/bin/env python
# coding: utf-8

import os
import numpy as np
import yaml
from collections import namedtuple

__all__ = []


## Object Factory
#
# We can define scenes by specifying object types and their parameters. 
# We will generate all object type definitions from setting default parameters.


DefaultCamera = {
    'position': [0.,0.,0.],
    'yaw': 0.0,
    'pitch': 0.0, 
    'width': 300,
    'height': 200,
    'field_of_view': 120.0,
    'focal_length': .01,
}



DefaultSphere = {
    'center': [0.,0.,0.], 
    'radius': 1.0, 
    'color': [1.,1.,1.], 
    'shininess': .5,
}


DefaultPlane = {
    'offset': 0.0, 
    'normal': [0.,0.,1.], 
    'color': [1.,1.,1.], 
    'shininess': 0.5,
}

DefaultTriangle = {
    'a': [0., 0., 0.],
    'b': [.5, 1., 0.],
    'c': [1., 0., 0.],
    'color': [1.,1.,1.], 
    'shininess': 1.0,
}


DefaultPointLight = {
    'position': [0.,0.,0.], 
    'ambient': [1.,1.,1.], 
    'diffuse': [1.,1.,1.], 
    'specular': [1.,1.,1.], 
    'brightness': 1.0
}
DefaultDirectionalLight = {
    'direction': [0.,0.,-1.], 
    'ambient': [1.,1.,1.], 
    'diffuse': [1.,1.,1.], 
    'specular': [1.,1.,1.], 
    'brightness': 1.0
}
#convert to numpy array and infer types and shapes
for var in dir():
    if var.startswith("Default"):
        varname, vardict = var[7:], eval(var)
        for param in vardict:
            value = eval(var+'["'+param+'"]')
            value = np.asarray(value)
            exec(var+'["'+param+'"] = value') 

all_object_types = []


#loop over previously defined defaults
for var in dir():
    if var.startswith("Default"):
        
        #get name and dictonary of parameters
        varname, vardict = var[7:], eval(var)
        params = [param for param in vardict]
        defaults = [vardict[param] for param in params]
        
        
        #generate struct-like object variable
        struct = namedtuple(varname, params, defaults=defaults)
        exec(varname + ' = struct')
        
        #publish object variable 
        all_object_types.append(struct)
        __all__.append(varname)
        
all_object_types = tuple(all_object_types)

#Consistency test
if __name__=='__main__':
    print('Object types:', __all__, '\n')


## Scene Parser
# 
# It is very convenient to define the scenes in YAML files.
# Also we would like the scene loader routine to initialize randomly if specified.
# 

def parse_scene(path, parse_counter=None):
    
    #use numerical data from YAML sheets
    with open(path) as f:
        description = yaml.full_load(f)
    
    #iteratively grow scene tree 
    scene = dict()
    for name in description:
        
        #infer scene object type
        assert 'object' in description[name], "Object '%s' has no object type." % name
        obj = description[name]['object']
        
        #Load parameters from dictionary
        params = description[name]
        params.pop('object')
        params = {param: np.array(params[param]) 
                  for param in params}
        
        if obj == 'Scene':
            assert 'file' in params, 'every scene needs to specify a file'
            other_scene = str(params['file'])
            other_scene = os.path.dirname(path) + os.sep + other_scene
            other_scene = parse_scene(other_scene)
            for name in other_scene:
                local_name = name
                if name in scene:
                    local_name = 'other_'+local_name
                scene[local_name] = other_scene[name] 
            continue
            
        
        
        #Read normally distributed scene parameters
        means, stds = dict(), dict()
        for param in params.copy():
            if param.endswith('_mean'):
                means[param[:-5]] = params[param]
                del params[param]
            elif param.endswith('_std'): 
                stds[param[:-4]] = params[param]
                del params[param]
        
        #Sample stochastic scene parameters
        for param in means:
            if param not in stds:
                stds[param] = np.ones(means[param].shape)
        for param in stds:
            if param not in means:
                means[param] = np.zeros(stds[param].shape)
        indices = [param for param in means]
        means = [means[param] for param in indices]
        stds  = [stds[param]  for param in indices]
        for param, mean, std in zip(indices, means, stds):
            params[param] = np.random.normal(loc=mean, scale=std)
        
        
        #Cast dtypes from Defaults 
        defaults = eval('Default' + obj)
        params = {
                param: np.asarray( params[param], dtype=defaults[param].dtype )
                for param in params
        }

        
        #Create scene objects 
        obj_type = eval(obj)
        try:
            scene[name] = obj_type(**params)
        except TypeError as e:
            msg = 'Error in object "%s".' % name
            msg += '\n'+ str(e)
            raise TypeError(msg)
        obj = scene[name]
        
        #Dimensionality check by comparing to Defaults
        for i,field in enumerate(obj._fields):
            ds = obj_type._fields_defaults[field].shape
            ps = eval('obj.'+field).shape
            
            assert ds == ps, 'Object "%s" has %s shape mismatch (%s not %s)' \
            % (name, field, str(ps), str(ds))
    
    return scene

#publish scene parser
__all__.append('parse_scene')

if __name__ == '__main__':
    
    def print_scene(scene_name):
        scene = parse_scene('../scenes/' +scene_name+ '.yaml')
        
        print('Scene "'+scene_name+'":')
        for name in scene:
            obj = scene[name]
            typ = type(scene[name])
            obj = obj._asdict()
            print(name, typ)
            for field in obj:
                print(' -', field, obj[field])
        
        print('')
    
    print_scene('small01')
    print_scene('small02')
    print_scene('small03')
    print_scene('small05')
