import re
import yaml

_float = r'([+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?)'
_vertex = r'v ' + _float + ' ' + _float + ' ' + _float 
#_normal = r'vn ' + _float + ' ' + _float + ' ' + _float 
#_triangle = r'f (\d+)//(\d+) (\d+)//(\d+) (\d+)//(\d+)'
_triangle = r'f (\d+) (\d+) (\d+)'


def read_wavefront_obj(path):
    vertices = []
    normals = []
    vert_indices = []
    norm_indices = []
    
    with open(path) as f:
        content = f.read()
    
    for _,line in enumerate(content.split('\n')):
        

        m = re.findall( _vertex, line)
        if len(m)>0:
            m = list(m[0])
            while '' in m: m.remove('')
            x,y,z = m[0], m[3], m[6]
            x,y,z = eval(x),eval(y),eval(z)
            vertices.append( [x,y,z] )

#        m = re.findall( _normal, line)
#        if len(m)>0:
#            m = list(m[0])
#            while '' in m: m.remove('')
#            x,y,z = m[0], m[3], m[6]
#            x,y,z = eval(x),eval(y),eval(z)
#            normals.append( [x,y,z] )
        
        m = re.findall( _triangle, line)
        if len(m)>0:
            m = list(m[0])            
            #va,na, vb,nb, vc,nc = m
            #na,va, nb,vb, nc,vc = m
            a,b,c = m
            
            #va,vb,vc = int(va)-1,int(vb)-1,int(vc)-1
            #na,nb,nc = int(na)-1,int(nb)-1,int(nc)-1
            #vert_indices.append( [va,vb,vc] )
            #norm_indices.append( [na,nb,nc] )
            a,b,c = int(a)-1, int(b)-1, int(c)-1
            vert_indices.append([a,b,c])
            
    #return vertices,normals,vert_indices,norm_indices
    return vertices,normals,vert_indices,norm_indices



if __name__ == '__main__':
    print('Unit Test: Wavefront Object Loader')
    
    path = '../meshes/tree_trunk.obj'
    vertices,normals,vert_indices,norm_indices = read_wavefront_obj(path)
    print('Found %d vertices, %d normals and %d triangles in "%s".' % 
          (len(vertices), len(normals), len(vert_indices), path))
    
    path = '../meshes/tree_leaves.obj'
    vertices,normals,vert_indices,norm_indices = read_wavefront_obj(path)
    print('Found %d vertices, %d normals and %d triangles in "%s".' % 
          (len(vertices), len(normals), len(vert_indices), path))
        
    
def generate_scene(filename,vertices, vert_indices, color=None, tag='mesh'):
#    scene = dict()
#    for _, (i,j,k) in enumerate(vert_indices):
#        tri = dict()
#        print('ijk=', (i,j,k))
#        tri['object'] = 'Triangle'
#        tri['a'] = vertices[i].copy()
#        tri['b'] = vertices[j].copy()
#        tri['c'] = vertices[k].copy()
#        print('triangle %d: ' % _)
#        print(tri['a'])
#        print(tri['b'])
#        print(tri['c'])
#        tri['color'] = color.copy()
#        scene[tag+'_tri_'+str(_)] = tri
#    scene = yaml.safe_dump(scene)
#    with open(filename, 'w') as f:
#        f.write(scene)
        
    s = '# automatically generated scene "%s" \n\n' % tag
    tab = '   '
    for _, (i,j,k) in enumerate(vert_indices):
        
        s += tag+'_tri_%d:\n' % _
        s += tab + 'object: %s\n' % "Triangle"
        s += tab + 'a: %s\n' % str(vertices[i])
        s += tab + 'b: %s\n' % str(vertices[j])
        s += tab + 'c: %s\n' % str(vertices[k])
        if color is not None:
            s += tab + 'color: %s \n' % str(color)
        s += '\n'
    
    with open(filename, 'w') as f:
        f.write(s)
        
if __name__ == '__main__':
    print('Unit Test: Scene Generator')
    
    path = '../meshes/cube.obj'
    vertices,normals,vert_indices,norm_indices = read_wavefront_obj(path)
    color = [.55, .39, .02]
    filename, tag = '../scenes/cube.yaml', 'cube'
    
    print('write %s to "%s"' % (tag, filename))
    generate_scene(filename, vertices, vert_indices, color, tag)
    
    
    path = '../meshes/teapot_reduced.obj'
    vertices,normals,vert_indices,norm_indices = read_wavefront_obj(path)
    filename, tag = '../scenes/teapot_reduced.yaml', 'teapot_reduced'
    print('write %s to "%s"' % (tag, filename))
    generate_scene(filename, vertices, vert_indices, None, tag) 
    
    
    path = '../meshes/tree_trunk.obj'
    vertices,normals,vert_indices,norm_indices = read_wavefront_obj(path)
    color = [.22, .29, .02]
    filename, tag = '../scenes/tree_trunk.yaml', 'trunk'
    print('write %s to "%s"' % (tag, filename))
    generate_scene(filename, vertices, vert_indices, color)
    
    path = '../meshes/tree_leaves.obj'
    vertices,normals,vert_indices,norm_indices = read_wavefront_obj(path)
    color = [.04, .74, .15]
    filename,tag = '../scenes/tree_leaves.yaml', 'leaves'
    print('write %s to "%s"' % (tag, filename))
    generate_scene(filename, vertices, vert_indices, color)
    
    