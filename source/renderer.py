#!/usr/bin/env python
# coding: utf-8

if __name__ == '__main__':
    import sys
    sys.path.append('..')
    import matplotlib.pyplot as plt

import source.tensorize as t
import torch.nn as nn
from source.scene import *
from source.geometry import *
from source.shader import *
from source.image import Image
import numpy as np

__all__ = ['FlatRenderer', 'PhongRenderer']



class AbstractRenderLayer(nn.Module):
    
    
    def clear(self):
        req_grad = self.requires_grad()
        self.depth_buffers = dict()
        self.color_buffers = dict()
        self.rays = dict()
        
        #clear buffers for each camera
        for camera_name in self.camera_names:
            camera = eval('self.'+camera_name)
            w,h = camera['width'], camera['height']
            depth_buffer = -t.ones( w*h, dtype=t.float, requires_grad=req_grad )
            color_buffer = t.zeros( (w*h,3), dtype=t.float, requires_grad=req_grad  )
            self.depth_buffers[camera_name] = depth_buffer
            self.color_buffers[camera_name] = color_buffer
            rays = construct_initial_rays(camera)
            self.rays[camera_name] = rays
            
    def __init__(self, scene, allowed_classes=None):
        nn.Module.__init__(self)
        
        #scene object type filters
        if allowed_classes is None:
            allowed_classes = [
                [Camera],
                [PointLight, DirectionalLight],
                [Sphere, Plane, Triangle]
            ]
        self.allowed_classes = allowed_classes
        camera_classes,light_classes,actor_classes = allowed_classes
        
        #store scene object names by category
        self.camera_names = []
        self.actor_names = []
        self.light_names = []
        
        self.scene_object_classes = dict()
        for name in scene:
            obj = scene[name]
            typ = type(obj).__name__
            self.scene_object_classes[name] = typ
        
        #iterate through scene objects and filter by type
        for name in scene:
            obj = scene[name]
            if any( [isinstance(obj, typ) for typ in camera_classes] ):
                self.camera_names.append(name)
            elif any( [isinstance(obj, typ) for typ in light_classes] ):
                self.light_names.append(name)
            elif any( [isinstance(obj, typ) for typ in actor_classes] ):
                self.actor_names.append(name)
            else:
                print('Object "%s" of type "%s" has no allowed scene type.' % \
                      (name, str(type(obj))) )
            
            #treat scene object as dictionary of fields
            vals= tensorize_scene_object(obj) 
            exec('self.'+name+'=nn.ParameterDict()')
            for field in vals:
                val = vals[field]
                
                #disable autograd by default
                val.requires_grad = False
                val = nn.Parameter( val, requires_grad=False )
                exec('self.'+name+'["'+field+'"] = val')
        
        #initialize output buffers
        self.clear()
        
    #this verbose method is helpful for debugging
    def __str__(self):
        msg = '%15s \t %15s \t %8s \t %8s\n' % ('name', 'dtype', 'device', 'grad') 
        msg += '-'*80 + '\n'
        
        for name in self.camera_names + self.actor_names + self.light_names:
            obj = eval('self.'+name)
            for field in obj:
                var = obj[field]
                key = name+'.'+field
                msg += '%-15s \t %15s \t %8s \t %8s\n' % \
                    (key, var.dtype, var.device, var.requires_grad) 
                    
        return msg
        
    
    #specify whether gradient is needed
    def set_trainable(self, params=None, trainable=True):
        for name in self.camera_names + self.actor_names + self.light_names:
            if params is None or name in params:
                obj = eval('self.'+name)
                for field in obj:
                    if field is None or field in params[name]:
                        if obj[field].dtype == t.float or obj[field].dtype == t.double:
                            exec('self.'+name+'["'+field+'"].requires_grad = trainable')
        self.clear()
                            
    def requires_grad(self):
        for name in self.camera_names + self.actor_names + self.light_names:
            obj = eval('self.'+name)
            for field in obj:
                if obj[field].requires_grad:
                    return True
        return False

    def render(self, camera_name, actor_name, subsampling):
        raise NotImplementedError('abstract methode, must be overridden by children!')

    def forward(self, subsampling=True):
        self.clear()
        
        images = dict()
        for camera_name in self.camera_names:
            for actor_name in self.actor_names:
                self.render(camera_name, actor_name, subsampling)

            camera = eval('self.'+camera_name)
            w, h = camera['width'], camera['height']
            images[camera_name] = Image(self.depth_buffers[camera_name], self.color_buffers[camera_name],
                                        camera_name, w, h)



                #return single image or dictionary of images
        if len( self.camera_names ) == 1:
            images = images[self.camera_names[0]]
        return images


if __name__ == '__main__':
    print('RenderLayer Unit Tests')
    print('==============================')
    
    scene = parse_scene('../scenes/small03.yaml')
    renderer = AbstractRenderLayer(scene)
    req_grad = renderer.requires_grad()
    print('Unit Test: parameter init, grad: %s' % req_grad)
    print(renderer)
    
    trainable_params = dict(
            ball   = ['center', 'radius', 'color'], 
            camera = ['focal_length'],
            light  = ['specular']
    )
    renderer.set_trainable(trainable_params)
    req_grad = renderer.requires_grad()
    print('Unit Test: after setting trainable, grad: %s' % req_grad)
    print(renderer)
    
    untrainable_params = dict(
            ball   = ['center'], 
            camera = ['focal_length'],
    )
    renderer.set_trainable(untrainable_params, trainable=False)
    req_grad = renderer.requires_grad()
    print('Unit Test: after setting untrainable, grad: %s' % req_grad)
    print(renderer)


class FlatRenderer(AbstractRenderLayer):

    def render(self, camera_name, actor_name, subsampling):
        actor = eval('self.'+actor_name)
        camera = eval('self.'+camera_name)
        w,h = camera['width'], camera['height']
        
        #calculate visibility
        rays = self.rays[camera_name]
        b_depth = self.depth_buffers[camera_name]
        typ = self.scene_object_classes[actor_name]
        if typ == 'Sphere':
            depth = intersect_rays_with_sphere(rays, actor)    
        elif typ == 'Plane':
            depth = intersect_rays_with_plane(rays, actor)    
        elif typ == 'Triangle':
            depth = intersect_rays_with_triangle(rays, actor)
        else:
            raise AttributeError('Scene Object %s has invalid type %s' % (actor_name, typ))
        mask = t.where((depth > 0) & ( (b_depth <= 0) | (depth < b_depth)), t.ones(1), t.zeros(1))

        box_filter = t.astensor([[-1., -1., -1.], [-1., 8., -1.], [-1., -1., -1.]])/9.
        box_filter = box_filter.reshape((1, 1, 3, 3))
        depth_image = mask.reshape(1, 1, w, h)
        edges = t.conv2d(depth_image, box_filter)[0,0,:,:]     
        edges = t.sqrt(edges**2 + 1e-6) - 1e-3

        b_depth = t.where(mask == 1., depth, b_depth)
        self.depth_buffers[camera_name] = b_depth

        b_color = self.color_buffers[camera_name]
        
        indices =  t.nonzero(edges) + 1
        center_indices = indices[:,0]*h + indices[:,1]
        left_indices   = (indices[:,0]-1)*h + indices[:,1]
        right_indices  = (indices[:,0]+1)*h + indices[:,1]
        bottom_indices = indices[:,0]*h + indices[:,1]-1
        top_indices = indices[:,0]*h + indices[:,1]-1
        
        #interpolation parameters
        n = 10
        ii,jj = np.mgrid[:n,:n]
        xx = 2.* ( t.astensor(ii,dtype=t.float) + .5)/n - 1.
        yy = 2.* ( t.astensor(jj,dtype=t.float) + .5)/n - 1.
        xx,yy = xx[None,:,:,None],yy[None,:,:,None]
        z = t.zeros((indices.shape[0], n,n,3))
        s,d = rays

        visibility = (mask > 0).float()
        if subsampling:
            #interpolated support
            s_sub = ((1.-xx)+(1.-yy)) * s[center_indices,None, None,:]
            s_sub = s_sub + t.where(xx > z, xx * s[right_indices,None, None,:],z)
            s_sub = s_sub - t.where(xx < z, xx * s[left_indices,None, None,:],z)
            s_sub = s_sub + t.where(yy > z, yy * s[top_indices,None, None,:],z)
            s_sub = s_sub - t.where(yy < z, yy * s[bottom_indices,None, None,:],z)
            s_sub = .5 * s_sub.reshape((-1,3))

            #interpolated direction
            d_sub = ((1.-xx)+(1.-yy)) * d[center_indices,None, None,:]
            d_sub = d_sub + t.where(xx > 0, xx * d[right_indices,None, None,:],z)
            d_sub = d_sub - t.where(xx < 0, xx * d[left_indices,None, None,:],z)
            d_sub = d_sub + t.where(yy > 0, yy * d[top_indices,None, None,:],z)
            d_sub = d_sub - t.where(yy < 0, yy * d[bottom_indices,None, None,:],z)
            d_sub = .5 * d_sub.reshape((-1,3))

            rays_sub = s_sub, d_sub
        
            if typ == 'Sphere':
                depth_sub = intersect_rays_with_sphere(rays_sub, actor)
            elif typ == 'Plane':
                depth_sub = intersect_rays_with_plane(rays_sub, actor)
            elif typ == 'Triangle':
                depth_sub = intersect_rays_with_triangle(rays_sub, actor)
            else:
                raise AttributeError('Scene Object %s has invalid type %s' % (actor_name, typ))

            mask_sub = depth_sub > 1e-3
            mask_sub = mask_sub.float()
            values = mask_sub.reshape((-1,n**2))
            values = t.sum(values, axis=-1)/n**2
            visibility[center_indices] = values
        
        #no lighting, here just flat
        albedo = actor['color']
        b_color = t.where(mask[:,None] == 1.,
                          visibility[:,None]*albedo[None,:]+(1.-visibility[:,None])*b_color, b_color)
        self.color_buffers[camera_name] = b_color

        b_color = t.where(mask[:, None] == 1.,
                          visibility[:, None] * albedo[None, :] + (1. - visibility[:, None]) * b_color, b_color)
        self.color_buffers[camera_name] = b_color


if __name__ == '__main__':
    scene = parse_scene('../scenes/small01.yaml')
    renderer = FlatRenderer(scene)
    
    print('Unit Test: Flat Shading Renderer')
    
    image = renderer.forward()
    image.show_image('depth')
    plt.show()
    image.show_image('albedo')
    plt.show()


class PhongRenderer(AbstractRenderLayer):
    

    def render(self, camera_name, actor_name, subsampling):
        actor = eval('self.'+actor_name)
        camera = eval('self.'+camera_name)
        w,h = camera['width'], camera['height']
        
        #calculate visibility
        rays = self.rays[camera_name]
        s,d = rays
        b_depth = self.depth_buffers[camera_name]
        actor_type = self.scene_object_classes[actor_name]
        if actor_type == 'Sphere':
            depth,normal = intersect_rays_with_sphere(rays, actor, return_normal=True)    
        elif actor_type == 'Plane':
            depth,normal = intersect_rays_with_plane(rays, actor, return_normal=True)
        elif actor_type == 'Triangle':
            depth,normal = intersect_rays_with_triangle(rays, actor, return_normal=True)
        else:
            print('Actor %s has invalid type %s' % (actor_name, actor_type))
        old_mask = b_depth > 1e-3
        mask = (depth > 0) & ( (b_depth <= 0) | (depth < b_depth))
        
#        #debug visibility
#        img = t.detach_numpy(mask).reshape(w,h).transpose()
#        plt.title('Intersection mask for actor "%s"' % actor_name)
#        plt.imshow(img, cmap='gray')
#        plt.show()
        
        b_depth = t.where(mask, depth, b_depth)
        self.depth_buffers[camera_name] = b_depth
        
        
        b_color = self.color_buffers[camera_name]
        b_color = t.where( (mask & old_mask)[:,None], t.astensor(0.), b_color)
        
        s_new = s + depth[:,None]*d 
        for light_name in self.light_names:
            light = eval('self.'+light_name)
            light_type = self.scene_object_classes[light_name]
            
            #light direction
            if light_type == 'PointLight':
                light_dir = s_new - light['position'][None, :] 
                light_dist = t.sqrt( t.sum(light_dir**2, axis=-1) + 1e-9)
                light_dir = -light_dir / light_dist[:,None]                
            elif light_type == 'DirectionalLight':
                light_dir = t.ones(s_new.shape) * light['direction'][None, :]
            else:
                print('Light %s has invalid type %s' (light_name, light_type))
            
            #shadow ray
            shadow = t.zeros(w*h, dtype=t.bool)
            for caster_name in self.actor_names:
                #if caster_name == actor_name: continue
                    
                caster = eval('self.'+caster_name)
                caster_type = self.scene_object_classes[caster_name]
                    
                if caster_type == 'Sphere':
                    caster_dist = intersect_rays_with_sphere( (s_new,light_dir), caster )
                elif caster_type == 'Plane':
                    caster_dist = intersect_rays_with_plane( (s_new,light_dir), caster )
                elif caster_type == 'Triangle':
                    caster_dist = intersect_rays_with_triangle( (s_new,light_dir), caster )
                
                covers =  (light_dist > caster_dist)  & (caster_dist > 1e-3) 
                shadow = shadow | covers
                
            #run phong shader to get color 
            color = actor['color']
            shininess = actor['shininess']
            ambient = light['ambient']
            diffuse = light['diffuse']
            specular = light['specular']
            lighting = phong(d, normal, light_dir, shadow, 
                            color, shininess,               #actor material                 
                            ambient, diffuse, specular )    #light quantaties

#            #debug light contribution 
#            l = t.where(mask[:,None], lighting, t.astensor(0.))
#            img = t.detach_numpy(l).reshape(w,h,3).transpose((1,0,2))
#            plt.title('contribution of light "%s"' % light_name)
#            plt.imshow(img, cmap='gray')
#            plt.show()
#            
            b_color = t.where(mask[:,None], b_color + lighting, b_color)

        self.color_buffers[camera_name] = b_color
        

if __name__ == '__main__':
    scene = parse_scene('../scenes/small01.yaml')
    renderer = PhongRenderer(scene)
    
    print('Unit Test: Phong Shading Renderer')
    
    image = renderer.forward()
    image.show_image('depth')
    plt.show()
    image.show_image('albedo')
    plt.show()
