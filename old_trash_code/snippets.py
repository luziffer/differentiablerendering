# -*- coding: utf-8 -*-

#view variable 
for name in ['variable',... ]:
    var = eval(name)
    print(name,':',type(var), var.dtype, var.shape)
    
    

# old triangle algorithm
#
#    equilateral triangle (a,b,c) 
#   
#    #test against three half planes
#    gamma,beta,alpha = (a+b)/2., (a+c)/2.-m, (b+c)/2.-m
#    cond_alpha = t.sum( (x - m[None,:]) * alpha[None,:], axis=-1) <= t.sum(alpha**2)
#    cond_beta  = t.sum( (x - m[None,:]) *  beta[None,:], axis=-1) <= t.sum(beta**2)
#    #cond_gamma = t.sum( (x - gamma[None,:]) * (gamma-m)[None,:], axis=-1) <  0.
#    intersection = intersection & cond_alpha
#    intersection = intersection & cond_beta
#    #intersection = intersection & cond_gamma
    
#    cond_a = t.sum( (x-m[None,:])*((a-m)[None,:]), axis=-1) > -1.
#    cond_b = t.sum( (x-m[None,:])*((b-m)[None,:]), axis=-1) > -1.
#    cond_c = t.sum( (x-m[None,:])*((c-m)[None,:]), axis=-1) > -1.
#    intersection = intersection & cond_a
#    intersection = intersection & cond_b
#    intersection = intersection & cond_c