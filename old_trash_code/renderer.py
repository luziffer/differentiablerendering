#!/usr/bin/env python
# coding: utf-8

if __name__ == '__main__':
    import sys
    sys.path.append('..')
    import matplotlib.pyplot as plt

import source.tensorize as t
import torch.nn as nn
from source.scene import *
from source.geometry import *

__all__ = ['FlatRenderer']



class AbstractRenderLayer(nn.Module):
    
    
    def clear(self):
        req_grad = self.requires_grad()
        self.depth_buffers = dict()
        self.color_buffers = dict()
        self.rays = dict()
        
        #clear buffers for each camera
        for camera_name in self.camera_names:
            camera = eval('self.'+camera_name)
            w,h = camera['width'], camera['height']
            depth_buffer = -t.ones( w*h, dtype=t.float, requires_grad=req_grad )
            color_buffer = t.zeros( (w*h,3), dtype=t.float, requires_grad=req_grad  )
            self.depth_buffers[camera_name] = depth_buffer
            self.color_buffers[camera_name] = color_buffer
            rays = construct_initial_rays(camera)
            self.rays[camera_name] = rays
            
    def __init__(self, scene, allowed_classes=None):
        nn.Module.__init__(self)
        
        #scene object type filters
        if allowed_classes is None:
            allowed_classes = [
                [Camera],
                [PointLight, DirectionalLight],
                [Sphere, Plane, Triangle]
            ]
        self.allowed_classes = allowed_classes
        camera_classes,light_classes,actor_classes = allowed_classes
        
        #store scene object names by category
        self.camera_names = []
        self.actor_names = []
        self.light_names = []
        
        self.scene_object_classes = dict()
        for name in scene:
            obj = scene[name]
            typ = type(obj).__name__
            self.scene_object_classes[name] = typ
        
        #iterate through scene objects and filter by type
        for name in scene:
            obj = scene[name]
            if any( [isinstance(obj, typ) for typ in camera_classes] ):
                self.camera_names.append(name)
            elif any( [isinstance(obj, typ) for typ in light_classes] ):
                self.light_names.append(name)
            elif any( [isinstance(obj, typ) for typ in actor_classes] ):
                self.actor_names.append(name)
            else:
                print('Object "%s" of type "%s" has no allowed scene type.' % \
                      (name, str(type(obj))) )
            
            #treat scene object as dictionary of fields
            vals= tensorize_scene_object(obj) 
            exec('self.'+name+'=nn.ParameterDict()')
            for field in vals:
                val = vals[field]
                
                #disable autograd by default
                val.requires_grad = False
                val = nn.Parameter( val, requires_grad=False )
                exec('self.'+name+'["'+field+'"] = val')
        
        #initialize output buffers
        self.clear()
        
    #this verbose method is helpful for debugging
    def __str__(self):
        msg = '%15s \t %15s \t %8s \t %8s\n' % ('name', 'dtype', 'device', 'grad') 
        msg += '-'*80 + '\n'
        
        for name in self.camera_names + self.actor_names + self.light_names:
            obj = eval('self.'+name)
            for field in obj:
                var = obj[field]
                key = name+'.'+field
                msg += '%-15s \t %15s \t %8s \t %8s\n' % \
                    (key, var.dtype, var.device, var.requires_grad) 
                    
        return msg
        
    
    #specify whether gradient is needed
    def set_trainable(self, params=None, trainable=True):
        for name in self.camera_names + self.actor_names + self.light_names:
            if params is None or name in params:
                obj = eval('self.'+name)
                for field in obj:
                    if field is None or field in params[name]:
                        if obj[field].dtype == t.float or obj[field].dtype == t.double:
                            exec('self.'+name+'["'+field+'"].requires_grad = trainable')
        self.clear()
                            
    def requires_grad(self):
        for name in self.camera_names + self.actor_names + self.light_names:
            obj = eval('self.'+name)
            for field in obj:
                if obj[field].requires_grad:
                    return True
        return False
    
        
    
    def forward(self, image_type='depth'):
        assert image_type in ['depth', 'albedo'], 'Unknown image output type %s' % image_type
        
        images = dict()
        for camera_name in self.camera_names:
            for actor_name in self.actor_names:
                for light_name in self.light_names:
                    self.render(actor_name, camera_name, light_name)
            
            camera = eval('self.'+camera_name)
            w,h = camera['width'], camera['height']
            if image_type == 'depth':
                images[camera_name] = self.depth_buffers[camera_name].reshape((w,h))
            elif image_type == 'binary':
                images[camera_name] = self.depth_buffers[camera_name].reshape((w,h)) > 1e-6
            elif image_type == 'albedo':
                images[camera_name] = self.color_buffers[camera_name].reshape((w,h,3)) 
        
        
        #return single image or dictionary of images
        if len( self.camera_names ) == 1:
            images = images[self.camera_names[0]]
        return images


if __name__ == '__main__':
    print('RenderLayer Unit Tests')
    print('==============================')
    
    scene = parse_scene('../scenes/small03.yaml')
    renderer = AbstractRenderLayer(scene)
    req_grad = renderer.requires_grad()
    print('Unit Test: parameter init, grad: %s' % req_grad)
    print(renderer)
    
    trainable_params = dict(
            ball   = ['center', 'radius', 'color'], 
            camera = ['focal_length'],
            light  = ['specular']
    )
    renderer.set_trainable(trainable_params)
    req_grad = renderer.requires_grad()
    print('Unit Test: after setting trainable, grad: %s' % req_grad)
    print(renderer)
    
    untrainable_params = dict(
            ball   = ['center'], 
            camera = ['focal_length'],
    )
    renderer.set_trainable(untrainable_params, trainable=False)
    req_grad = renderer.requires_grad()
    print('Unit Test: after setting untrainable, grad: %s' % req_grad)
    print(renderer)



class FlatRenderer(AbstractRenderLayer):

    def render(self, actor_name, camera_name, light_name):
        actor = eval('self.'+actor_name)
        camera = eval('self.'+camera_name)
        w,h = camera['width'], camera['height']
        
        #calculate visability
        rays = self.rays[camera_name]
        b_depth = self.depth_buffers[camera_name]
        typ = self.scene_object_classes[actor_name]
        if typ == 'Sphere':
            depth = intersect_rays_with_sphere(rays, actor)    
        elif typ == 'Plane':
            depth = intersect_rays_with_plane(rays, actor)    
        elif typ == 'Triangle':
            depth = intersect_rays_with_triangle(rays, actor)
        else:
            print('Scene Object %s has invalid type %s' % (actor_name, typ))
        mask = (depth > 0) & ( (b_depth <= 0) | (depth < b_depth))
        
        #img = mask.detach().reshape((w,h)).cpu().numpy().transpose()
        #plt.imshow( img, cmap='gray' )
        #plt.title('Actor %s, light %s' % (actor_name,light_name))
        #plt.show()
        
        b_depth = t.where(mask, depth, b_depth)
        self.depth_buffers[camera_name] = b_depth
        
        #add lighting, here just flat
        light = eval('self.'+light_name)
        b_color = self.color_buffers[camera_name]
        albedo = actor['color']
        b_color = t.where(mask[:,None], albedo[None,:], b_color)
        self.color_buffers[camera_name] = b_color

if __name__ == '__main__':
    scene = parse_scene('../scenes/small04.yaml')
    renderer = FlatRenderer(scene)
    
    image = renderer.forward('depth')
    image = image.detach().cpu().numpy().transpose()
    plt.imshow(image, cmap='gray')
    plt.show()

    image = renderer.forward('albedo')
    image = image.detach().cpu().numpy().transpose((1,0,2))
    plt.imshow(image)
    plt.show()



#class SilhuetteRenderer(AbstractRenderLayer):
    
    
    
#    def __init__(self, scene, allowed_classes=None): #more hyper-parameters to follow
#        AbstractRenderLayer.__init__(self, scene, allowed_classes)
#        self.clear()
#
#    def clear(self):
#        req_grad = self.requires_grad()
#        self.depth_buffers = dict()
#        self.color_buffers = dict()
#        self.rays = dict()
#        for camera_name in self.camera_names:
#            camera = eval('self.'+camera_name)
#            w,h = camera['width'], camera['height']
#            depth_buffer = -t.ones( w*h, dtype=t.float, requires_grad=req_grad )
#            color_buffer = t.zeros( (w*h,3), dtype=t.float, requires_grad=req_grad  )
#            self.depth_buffers[camera_name] = depth_buffer
#            self.color_buffers[camera_name] = color_buffer
#            rays = construct_initial_rays(camera)
#            self.rays[]
#    
#    def init_rays(self):
#        req_grad = self.requires_grad()
#        self.depth_buffers = dict()
#        self.color_buffers = dict()
#        for camera_name in self.camera_names:
#            camera = eval('self.'+camera_name)
#        
#    
#    
#    def forward_actor(self, actor_name):
#        assert actor_name in self.actor_names, 'Invalid actor %s' % actor_name
#        actor = eval('self.'+actor_name)
#        typ = self.scene_object_classes[actor_name]
#        
#        
#        if rays is None:
#            print('Actor "%s" called forward_ | rays= ', rays)
#            rays = self.rays
#           
#         
#        
##        if typ == 'Sphere':
##            depth = intersect_rays_with_sphere(rays, actor)    
##        elif typ == 'Plane':
##            depth = intersect_rays_with_plane(rays, actor)    
##        elif typ == 'Triangle':
##            depth = intersect_rays_with_triangle(rays, actor)
##        else:
##            print('Scene Object %s has invalid type %s' % (actor_name, typ))
##        
##        if depth_buffers is None:
##            depth_buffers = {c: t.ones for c om self.camera_names}
##        for camera_name in self.camera_names:
##            camera = eval('self.'+camera_name)
##            w,h = camera['width'], camera['height']
##            req_grad = self.requires_grad()
#            
##    
##        
##    
##    def forward(self, output='binary'):
##        image_types = ['binary', 'albedo', 'depth']
##        assert output in image_types, '"%s" is not a valid output image option.'%output + \
##        'Use one of "%s"' % ', '.join(image_types)
##        image_type = output
##        
##        images = dict()
##        for camera_name in self.camera_names:
##            camera = eval('self.'+camera_name)
##            w,h = camera['width'], camera['height']
##            req_grad = self.requires_grad()
##            depth_buffer = t.zeros( w*h, dtype=t.float, requires_grad=req_grad )
##            color_buffer = t.zeros( (w*h,3), dtype=t.float, requires_grad=req_grad  )
##            s,d = construct_initial_rays(camera)
##            rays = ( s.reshape((w*h, -1)), d.reshape((w*h, -1)) )
##            
##            for actor_name in self.actor_names:
##                typ = self.scene_object_classes[actor_name]
##                actor = eval('self.'+actor_name)
##                
##                if typ == 'Sphere':
##                    depth = intersect_rays_with_sphere(rays, actor)    
##                elif typ == 'Plane':
##                    depth = intersect_rays_with_plane(rays, actor)    
##                elif typ == 'Triangle':
##                    depth = intersect_rays_with_triangle(rays, actor)
##                else:
##                    print('Scene Object %s has invalid type %s' % (actor_name, typ))
##                
##                mask = (depth > 0) & ( (depth_buffer <= 0) | (depth<depth_buffer))
##                depth_buffer = t.where(mask, depth, depth_buffer)
##                color_buffer = t.where(mask[:,None], actor['color'][None,:], color_buffer)
##            
##                # edge detection and subsampling there for differentiability!!
##            
##            
##            if image_type == 'depth':
##                images[camera_name] = depth_buffer.reshape((w,h))
##            elif image_type == 'binary':
##                images[camera_name] = depth_buffer.reshape((w,h)) > 1e-6
##            elif image_type == 'albedo':
##                images[camera_name] = color_buffer.reshape((w,h,3))
##            
##        if len( self.camera_names ) == 1:
##            images = images[self.camera_names[0]]
##        return images
#    
##Register renderer    
#__all__.append('SilhuetteRenderer')
#
#if __name__ == '__main__':
#    print('SilhuetteRenderer Unit Tests')
#    print('==============================')
#    import numpy as np
#    import matplotlib.pyplot as plt
#    
#    
#    scene = parse_scene('../scenes/small04.yaml')
#    
#    renderer = SilhuetteRenderer(scene)
#    print('Grad required: ', renderer.requires_grad())
#    
#    img = renderer.forward('binary')
#    img = img.cpu().numpy().transpose()
#    plt.imshow(img, cmap='gray', vmin=0, vmax=1)
#    plt.title('Binary')
#    plt.show()
#    
#    img = renderer.forward('depth')
#    img = img.cpu().numpy().transpose()
#    plt.imshow(img, cmap='gray', vmin=0, vmax=8)
#    plt.title('Depth')
#    plt.show()
#    
#    img = renderer.forward('albedo')
#    img = img.cpu().numpy().transpose((1,0,2))
#    plt.imshow(img)
#    plt.title('Albedo')
#    plt.show()