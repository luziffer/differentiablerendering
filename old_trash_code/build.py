#!/usr/bin/env python
# coding: utf-8

# # Utility Source Builder
# 
# Run this script to generate *.py source files from *.ipynb documentations.


import os 
import shutil
import json


# ### Identify Notebooks
# 
# Copy all notebooks from the utils folder to build cache.  


#Create build cache folder
if 'build' in os.listdir():
    shutil.rmtree('build')
os.mkdir('build')

#Loop over notebooks
for f in os.listdir():
    name, ext = os.path.splitext(f)
    if ext=='.ipynb' and name != 'build':
        #os.system('nbconvert %s --to py ')
        
        print('Identified notebook %s' % f)
        shutil.copy( f, 'build/' + f)


# ### Remove  Unit Tests 

#Loop over all notebooks
for f in os.listdir('build'):
    f = 'build/'+f
    print('\nProcessing notebook %s' % f)
    
    with open(f, 'r') as stream:
        notebook = stream.read()
    notebook = json.loads(notebook)
    
    #Find cells that reference the __name__ execution context
    #Caveat: this is sensitive to intendation!
    cells = notebook['cells']
    src_i = 0
    for i,cell in enumerate(cells):
        if cell['cell_type'] == 'code':
            src_i += 1
            
            lines = cell['source']
            unit_test_flag = False
            for line in lines:
                if line.startswith('if __name__'):
                    unit_test_flag = True
            
            #Remove unit tests for faster execution
            if unit_test_flag:
                
                cell['source'] = ['# <Unit Test Autoremoved>']
                print('Unit test In [',cell['execution_count'], ']: Wiped source cell %d.' % (src_i))
                cell['outputs'] = []
                
            cells[i] = cell
    notebook['cells'] = cells
        
    notebook = json.dumps(notebook)
    with open(f, 'w') as stream:
        stream.write(notebook)
    


# ### Convert Notebooks

for f in os.listdir('build'):
    name,ext = os.path.splitext(f)
    g = '%s.py' % name
    if ext == '.ipynb':
        print('Generating source %s' % g)
        os.system('jupyter nbconvert %s --to python %s.py' % ('build/'+f, 'build/'+g) )
        
        shutil.copy('build/' + g, g)


# ### Clean Up

shutil.rmtree('build')

