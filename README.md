# DifferentiableRendering

A Differentiable Raytracing Renderer allows using it as a layer in a Deep Neural Network.
The project consists of these major parts:

1. Implementing a General ray-tracing rendering algorithm
2. Test differentiabilty by using automatic differntiation
3. Learn unknown scene parameters with gradient descent
    

Take a look at the notebooks for further information.